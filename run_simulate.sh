#!/bin/bash

python simulate_vcf.py \
	--signatures SBS3 SBS8 \
	--activities 0.2 0.8 \
	--hg19 ~/workspace/CSI_work/hg19.fa \
	--hgver hg19 \
	--N_samples 2 \
	--N_mut 10000 \
	--N_noise 1000 \
	--FCs 2.0 3.0 \
	--cosmicver 3.0 \
	--feature data/ctcf_sites_mcf7.bed \
	--output simulated_out