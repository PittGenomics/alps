<p align="center"><img width=50% src="https://gitlab.com/PittGenomics/alps/-/blob/main/media/alps_logo.png"></p>

## What is ALPS?

**ALPS** (**A**ssignment of **L**ocal **P**robabilities for SBS **S**ignatures) is a probabilistic framework for assignment of SBS mutational signature enrichments at genomic and epigenomic features of interest. 

## Installation

Clone the ALPS repository to a local folder. Use the file `requirements.txt` to create a `conda` environment:

```
conda create --name alps --file requirements.txt
conda activate alps
```

## SBS fold change calculation

`bedtools` should be installed in the environment where the scripts are executed.
Input folder should contain `.vcf` files; feature is a `.bed` file with genomic features of interest. 
Reference genome should be provided. Activities of COSMIC signatures should be pre-assigned and supplemented with the `--activities` argument (`Assignment_Solution_Activities.txt` from `SigProfilerAssignment`).

**ALPS** consists of two analysis steps: 

**1)** `alps ssbootstrap` for mutational occupancy calculation and single-sample bootstrap fold change computation

**2)** if there are multiple samples being analysed, `alps multisample` is used to compute the overall fold change of each active SBS signature at the feature of interest. The more samples are analysed, the higher the statistical power of `alps multisample`. 

**(*)** Additionally, the user can simulate their custom `.vcf` samples with `alps simulate` using specified fold changes of one or more SBS with independently specified activities at the genomic feature of interest.

1. `alps ssbootstrap` will calculate the mutational burden and single-sample fold change enrichment for the genomic features supplied in the `.bed` file. This file will produce temporary files with matrices of weighted mutational burden per bedfile entry for each active SBS in the sample, and then compute the FC for every sample and save in a separate `.csv` file. Parameters:

| Argument | Default value | Description
| --- | --- | --- |
| `--input` | None | (required) input folder with `.vcf` files |
| `--feature` | None | (required) `.bed` feature file path |
| `--out`  | None | (required) output folder name |
| `--hg` | None | (required) path to reference genome `.fa` |
| `--hgver` | None | (required) hg version, either hg19 or hg38 |
| `--activities` | None | (required) file with COSMIC activities for given samples |
| `--cosmicver` | 3.0 | COSMIC version, either 3.3, 3.0, 2.0 or PCAWG |
| `--perm` | 5 | number of permutations for bedtools shuffle |
| `--temp_from_var` | False | use alternative temporary folder location |
| `--temp_folder` | None | temporary folder location (if `--temp_from_var` is set) |
| `--correct_trinucl` | False | correct for trinucleotide context |
| `--pad` | 0 | padding for the bed features |
| `--p_cutoff` | 0.05 | p-value cutoff for statistical tests on significance |
| `--N_perm` | 1000 | number of bootstrapping experiments |
| `--N_sampled` | 10000 | number of samples in one bootstrap |

Example of usage:

```bash
alps ssbootstrap \
    --input data/snv_BRCA \
    --feature data/ctcf_sites_mcf7.bed \
    --hg data/hg19.fa \
    --hgver hg19 \
    --activities data/valid_pcawg_published_activities_BRCA.txt \
    --out out \
```

For each input sample, a separate output file will be created with mean bootstrapped fold changes and the respective p-values. The `.csv` will contain columns for all SBS active in the set of input samples, but if no mutations of the given SBS fell into the feature, the FC and p-value fields will be empty.

| index | FC | p-value | 
| --- | --- | --- | 
| SBS1 | 2.661540179978400 | 1.90752166217185E-104 |
| SBS2 |	4.752166279999660 | 7.94828584143499E-11 |
| SBS3 | 1.131259198379820 | 5.08843620012689E-36 |
| SBS5 | 0.8688320098203990 | 2.68023634239794E-121 |
| SBS9 |  |  |
| SBS13 | 14.771790321990700 | 6.80518150687188E-121 | 

2. If there are multiple samples analysed, the final FC will be computed and plotted with the command `alps multisample` and saved to the output folder. Additionally, a Mann-Whitney U test will be run on each set of fold changes per SBS and log2(FC) and compared with 1 to test whether the fold change is significant on either side. p-value cutoff parameter here is for the Mann-Whitney U test. All arguments:

| Argument | Default value | Description
| --- | --- | --- |
| `--folder` | None | (required) folder(s) with generated single-sample FC |
| `--output`  | None | (required) output folder name (can be the same as `--folder`) |
| `--activities` | None | (required) file with COSMIC activities for given samples |
| `--p_cutoff` | 0.05 | p-value cutoff for statistical tests on significance |

Example of usage:

```bash
alps multisample \
    --folder out_batch1 out_batch2 \
    --activities data/valid_pcawg_published_activities_BRCA.txt \
    --output out \
```
Output: this command will create files `all_SBS_intersample.csv` with the mean fold changes per SBS, weighted by the sample-wise activity, and a `FC_heatmap.pdf` plot with log $_{2}$ FC boxplots and their weighted means. 

Different SBS may have dramatically different activities in different samples, and to calculate a fairer total FC, each fold change is multiplied by the activity of the SBS in that sample, and the sum of weighted FC is then divided by the sum of SBS activities in all analysed samples: $FC = \frac{\sum FC_{i}*a_{i}}{\sum a_{i}}$. 

p-value in the `all_SBS_intersample.csv` file corresponds to the Mann-Whitney U test of the log $_{2}$ FCs and a control group of identity fold changes (FC = 1). If the p-value does not pass the specified threshold of significance (0.05 by default), the value will not be shown in the `FC_heatmap.pdf` mean column.

## Synthetic `.vcf` simulation -- `alps simulate`

ALPS has an inbuilt `.vcf` simulation ran with `alps simulate` for the purposes of building a reliable control. The simulation will take a `.bed` file for the input, the list of signatures and their activities to simulate, number of mutations and SBS fold changes at the supplied genomic feature. All arguments:

| Argument | Default value | Description
| --- | --- | --- |
| `--output` | None | (required) output folder name |
| `--signatures` | None | (required) signatures to simulate |
| `--activities` | None | (required) activities to simulate; must have the same length as signatures |
| `--FCs`  | None | (required) FCs to simulate; must have the same length as signatures |
| `--feature` | None | (required) genomic feature to populate |
| `--hg19` | None | (required) path to hg19.fa |
| `--hgver` | None | (required) hg version, either hg19 or hg38 |
| `--N_samples` | 1 | number of sample to simulate |
| `--N_mut` | 10000 | number of mutations to populate per sample |
| `--N_noise` | 1000 | number of noise mutations |
| `--cosmicver` | 3.0 | COSMIC version, either 3.3, 3.0, 2.0 or PCAWG |

Example of usage:

```bash
alps simulate \
	--signatures SBS3 SBS8 \
	--activities 0.2 0.8 \
	--hg19 ../../hg19.fa \
	--hgver hg19 \
	--N_samples 3 \
	--N_mut 10000 \
	--N_noise 1000 \
	--FCs 2.0 3.0 \
	--cosmicver 3.0 \
	--feature data/ctcf_sites_mcf7.bed \
	--output simulated_out
```

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.
