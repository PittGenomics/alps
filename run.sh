#!/bin/bash

stage=0
stop_stage=1

. ./parse_options.sh || exit 1;

out_folder="out_CTCF_BRCA"
hg_file="/Users/anyak/workspace/CSI_work/hg19.fa"
activities="data/valid_pcawg_published_activities_BRCA.txt"

if [ ${stage} -le 0 ] && [ ${stop_stage} -ge 0 ]; then

	python calc_occupancy_bootstrap.py \
		--input data/snv_BRCA \
		--feature data/ctcf_sites_mcf7.bed \
		--hg $hg_file \
		--hgver hg19 \
		--activities $activities \
		--out $out_folder \

fi

if [ ${stage} -le 1 ] && [ ${stop_stage} -ge 1 ]; then

	python multisample_bootstrap.py \
		--folder $out_folder \
		--activities $activities \
		--output $out_folder \

fi