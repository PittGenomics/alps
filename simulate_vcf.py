from Bio import SeqIO
from tqdm import tqdm

import os
import argparse
import sys
import re
import subprocess
from re import finditer
import pandas as pd
import numpy as np
import random
import shutil
import gc


# check arguments
def check_argv():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--signatures", nargs="+", type=str, help="signatures to simulate"
    )
    parser.add_argument(
        "--activities",
        nargs="+",
        type=float,
        help="activities to simulate; must have the same length as signatures",
    )
    parser.add_argument(
        "--FCs",
        nargs="+",
        type=float,
        help="FCs to simulate; must have the same length as signatures",
    )
    parser.add_argument(
        "--feature",
        required=False,
        type=str,
        help="genomic feature to populate",
    )
    parser.add_argument(
        "--N_samples",
        default=1,
        required=True,
        type=int,
        help="number of sample to simulate",
    )
    parser.add_argument(
        "--N_mut",
        default=5000,
        required=True,
        type=int,
        help="approximate number of mutations to populate per sample",
    )
    parser.add_argument(
        "--N_noise",
        default=1000,
        required=True,
        type=int,
        help="number of noise mutations",
    )
    parser.add_argument(
        "--hg19", default="hg19.fa", required=True, type=str, help="path to hg19.fa"
    )
    parser.add_argument("--hgver", help="hg version, either hg19 or hg38")
    parser.add_argument(
        "--cosmicver",
        required=True,
        help="COSMIC version, either 3.3, 3.0, 2.0 or PCAWG",
    )
    parser.add_argument(
        "--output",
        default="out",
        required=True,
        type=str,
        help="output folder name",
    )

    return parser.parse_args()


def rev_compl_mut(mut_tuple):
    rev_dict = {"A": "T", "T": "A", "C": "G", "G": "C", "N": "N"}
    ini, mut = mut_tuple
    ini_ = "".join([rev_dict[l] for l in ini])[::-1]
    mut_ = "".join([rev_dict[l] for l in mut])[::-1]

    return (ini_, mut_)


def rev_compl(s):
    rev_dict = {"A": "T", "T": "A", "C": "G", "G": "C", "N": "N"}
    s_ = "".join([rev_dict[l] for l in s])[::-1]
    return s_


def add_noise(N_noise, seq):
    ini = []
    mut = []
    loci = []
    all_nucl = ["A", "C", "T", "G"]
    for _ in range(int(N_noise)):
        r = random.randrange(len(seq))
        N = seq[r]
        if N != "N":
            left = [x for x in all_nucl if x != N]
            N_new = random.sample(left, 1)
            ini.append(N)
            mut.append(N_new[0])
            loci.append(r)
    return (ini, mut, loci)


dict_hgver = {"hg19": "GRCh37", "hg38": "GRCh38"}

args = check_argv()

assert len(args.signatures) == len(
    args.activities
), f"expected the same number of signatures and activities, got {len(args.signatures)} and {len(args.activities)}"

assert len(args.signatures) == len(
    args.FCs
), f"expected the same number of signatures and activities, got {len(args.signatures)} and {len(args.FCs)}"

activities = [a / np.sum(np.array(args.activities)) for a in args.activities]

if args.cosmicver == "3.3":
    df = pd.read_csv(f"data/COSMIC_v3.3.1_SBS_{dict_hgver[args.hgver]}.txt", sep="\t")
elif args.cosmicver == "3.0":
    df = pd.read_csv(f"data/COSMIC_v3_SBS_{dict_hgver[args.hgver]}.txt", sep="\t")
elif args.cosmicver == "2.0":
    df = pd.read_csv(f"data/COSMIC_v2_SBS_{dict_hgver[args.hgver]}.txt", sep="\t")
elif args.cosmicver == "PCAWG":
    df = pd.read_csv(f"data/pcawg_published_reference.txt", sep="\t")

df["Type1"] = [
    (
        s.split("[")[0] + s.split("[")[1][0] + s.split("]")[-1],
        s.split("[")[0] + s.split("]")[0][-1] + s.split("]")[-1],
    )
    for s in df["Type"]
]

df.set_index("Type1", inplace=True)
df.drop("Type", axis=1, inplace=True)

all_sbs = [x for x in list(df.columns) if "SBS" in x]

genome_length = pd.read_csv(
    f"data/{args.hgver}.genome", sep="\t", header=0, index_col=None
)
tot_length_genome = genome_length["size"].sum()
genome_length["fraction"] = [x / tot_length_genome for x in genome_length["size"]]

feature = pd.read_csv(args.feature, sep="\t", header=None)
feature["size"] = feature[2] - feature[1]
feature_length = feature["size"].sum()

proc = subprocess.Popen(["which", "shuffleBed"], stdout=subprocess.PIPE)
out = proc.stdout.read().decode("utf-8")
bedtools_exec = "/".join(out.strip("\n").split("/")[:-1])
print("bedtools executable path to be used:", bedtools_exec)

# uncomment if this is not a test run and you want to process all chromosomes
region = [
    "chr1",
    "chr2",
    "chr3",
    "chr4",
    "chr5",
    "chr6",
    "chr7",
    "chr8",
    "chr9",
    "chr10",
    "chr11",
    "chr12",
    "chr13",
    "chr14",
    "chr15",
    "chr16",
    "chr17",
    "chr18",
    "chr19",
    "chr20",
    "chr21",
    "chr22",
    "chrX",
    "chrY",
]

# dictionary to store hg19 sequences
seq_dict = {}
total_length = 0

with open(args.hg19, mode="r") as handle:
    # process each record in .fa file if there's more than one
    for record in SeqIO.parse(handle, "fasta"):
        identifier = record.id
        description = record.description
        # ignore alternative contigs
        if identifier in region:
            print(description)
            sequence = record.seq
            seq_dict[identifier] = str(sequence)
            total_length += len(sequence)

# create output directory if it doesn't exist
if not os.path.exists(args.output):
    os.makedirs(args.output)

if not os.path.exists(os.path.join(args.output, "temp")):
    os.makedirs(os.path.join(args.output, "temp"))

temp_tosave = os.path.join(args.output, "temp")

# list of mutation types in tuples; ('CTA', 'CAA') means from 'CTA' > to 'CAA'
tuples_mut = [
    (f"{b5}{x}{b3}", f"{b5}{y}{b3}")
    for b5 in "AGTC"
    for x, y in zip(["C", "C", "C", "T", "T", "T"], ["A", "G", "T", "A", "C", "G"])
    for b3 in "AGTC"
]

channels = [x[0] for x in tuples_mut]

# convert into dict and assign numeric mutation codes
dict_mut = {k: v for k, v in zip(tuples_mut, range(len(tuples_mut)))}
df_mut = pd.DataFrame({k: [dict_mut[k]] for k in dict_mut.keys()})
# save the numeric codes of single base mismatches
df_mut.to_csv(os.path.join(args.output, "sbs_codes.csv"))

for sample_no in range(args.N_samples):
    loci, chs = [], []
    ini_all_noise, mut_all_noise, loci_all_noise, chrom_noise = [], [], [], []
    sampled_feat = {x: 0 for x in df.index}

    for k, sign in enumerate(args.signatures):
        feature_dict = {}
        feature_length = 0
        print(f"......Processing genomic feature.......")
        feature_dict = {chr_id: {} for chr_id in feature[0].unique()}
        # loop over chromosomes: whatever we saved into seq_dict according to chromosomal region of interest
        for chr_id, start, end in tqdm(zip(feature[0], feature[1], feature[2])):
            ref = seq_dict[chr_id][start:end].upper()
            feature_length += len(ref)
            for x in range(1, len(ref) - 2):
                feature_dict[chr_id][start + x] = ref[x - 1 : x + 2]

        inverse_dict_feature = {x: [] for x in channels}

        for chr_id in tqdm(feature_dict.keys()):
            print(f"......Inverting feature dictionary {chr_id}.......")
            for loc in feature_dict[chr_id].keys():
                if feature_dict[chr_id][loc] in inverse_dict_feature.keys():
                    inverse_dict_feature[feature_dict[chr_id][loc]].append(
                        (chr_id, loc, "+")
                    )
                if rev_compl(feature_dict[chr_id][loc]) in inverse_dict_feature.keys():
                    inverse_dict_feature[rev_compl(feature_dict[chr_id][loc])].append(
                        (chr_id, loc, "-")
                    )

        N_feat = {sign: []}
        N_bck = {sign: []}

        N_mut_signature = args.N_mut * args.activities[k]
        N_bck[sign] = N_mut_signature / (
            1 + args.FCs[k] * feature_length / (total_length - feature_length)
        )
        N_feat[sign] = N_mut_signature - N_bck[sign]

        signature_dict = df[sign]
        signature_dict_filt = signature_dict[signature_dict > 0.01]
        signature_dict_filt = signature_dict_filt * 100
        list_to_sample_ch = []
        for ch, v in zip(signature_dict_filt.index, signature_dict_filt.values):
            list_to_sample_ch.extend([ch] * int(v))

        sampled_ch = random.choices(list_to_sample_ch, k=int(np.ceil(N_feat[sign])))
        for ch in sampled_ch:
            sampled_feat[ch] += 1

        print("Background mutations:", N_bck, ", feature mutations:", N_feat)
        projected_FC = (N_feat[sign] / feature_length) / (
            N_bck[sign] / (total_length - feature_length)
        )
        print(
            "Background length:",
            total_length,
            ", feature length:",
            feature_length,
            "FC:",
            projected_FC,
        )

        # loop over chromosomes: whatever we saved into seq_dict according to chromosomal region of interest
        for chr_id in tqdm(region):
            df_chr_temp = pd.DataFrame(
                {"chr": [chr_id], "start": [0], "end": [len(seq_dict[chr_id]) - 1]}
            )
            df_chr_temp.to_csv(
                os.path.join(temp_tosave, f"{chr_id}.bed"),
                sep="\t",
                index=None,
                header=None,
            )

            f = open(f"{temp_tosave}/{chr_id}_nointersect.bed", "w")
            subprocess.call(
                [
                    f"{bedtools_exec}/subtractBed",
                    "-a",
                    f"{temp_tosave}/{chr_id}.bed",
                    "-b",
                    args.feature,
                ],
                stdout=f,
            )

            chr_bed = pd.read_csv(
                f"{temp_tosave}/{chr_id}_nointersect.bed", sep="\t", header=None
            )

            # remove "chr" from chromosome name if needed
            print(f"......Processing chromosome {chr_id}.......")

            inverse_dict = {x: [] for x in channels}

            print("Search for all trinucleotide channels...")
            for start, end in tqdm(zip(chr_bed[1], chr_bed[2])):
                ref = seq_dict[chr_id][start:end].upper()
                for ch in inverse_dict.keys():
                    for m in finditer(ch, ref):
                        inverse_dict[ch].append((chr_id, m.start() + 1, "+"))
                    for m in finditer(rev_compl(ch), ref):
                        inverse_dict[ch].append((chr_id, m.start() + 1, "-"))

            N_bck = {sign: []}

            frac = float(genome_length[genome_length["chrom"] == chr_id]["fraction"])
            N_mut_signature = args.N_mut * args.activities[k] * frac
            print(f"Background mutations in {chr_id}: {N_mut_signature}")
            N_bck[sign] = N_mut_signature / (
                1 + args.FCs[k] * feature_length / (total_length - feature_length)
            )

            # now we need to count mutation types by channel from the COSMIC table and sample the respective trinucl,
            # populate a new dictionary with the trinucl location and (from)>(to) channels for the final vcf
            sampled_bck = {x: 0 for x in df.index}

            signature_dict = df[sign]
            signature_dict_filt = signature_dict[signature_dict > 0.01]
            signature_dict_filt = signature_dict_filt * 100
            list_to_sample_ch = []
            for ch, v in zip(signature_dict_filt.index, signature_dict_filt.values):
                list_to_sample_ch.extend([ch] * int(v))

            sampled_ch = random.choices(list_to_sample_ch, k=int(np.ceil(N_bck[sign])))
            for ch in sampled_ch:
                sampled_bck[ch] += 1

            # select channels by upscaling the mutational profile
            # for ch, v in zip(signature_dict.index, signature_dict.values):
            #    N_b = N_bck[sign] * v
            #    if N_b > 0:
            #        sampled_bck[ch] += N_b

            # sampling
            for ch in sampled_bck.keys():
                chs.extend([ch] * int(np.ceil(sampled_bck[ch])))
                loci.extend(
                    random.sample(inverse_dict[ch[0]], int(np.ceil(sampled_bck[ch])))
                )

            N_noise_chr = np.floor(args.N_noise * frac)
            if k == 0:
                if N_noise_chr > 0:
                    ini_noise, mut_noise, loci_noise = add_noise(
                        N_noise_chr, seq_dict[chr_id].upper()
                    )
                    ini_all_noise.extend(ini_noise)
                    mut_all_noise.extend(mut_noise)
                    loci_all_noise.extend(loci_noise)
                    chrom_noise.extend([chr_id] * len(loci_noise))

            print(
                f"Background processing finished; loci length: {len(loci)} including noise: {N_noise_chr}"
            )

            collected = gc.collect()
            print(f"Garbage collector: collected {collected} objects")

    for ch in sampled_feat.keys():
        chs.extend([ch] * int(np.ceil(sampled_feat[ch])))
        loci.extend(
            random.sample(inverse_dict_feature[ch[0]], int(np.ceil(sampled_feat[ch])))
        )

    # VCF: CHROM POS ID REF ALT QUAL FILTER INFO
    # 1 2261023 .   C   G   .   .   Callers=dkfz
    df_vcf = pd.DataFrame(
        {
            "chr": [x[0][3:] for x in loci] + [x[3:] for x in chrom_noise],
            "pos": [x[1] + 1 for x in loci] + [x + 1 for x in loci_all_noise],
            "ID": ["."] * len(loci) + ["."] * len(loci_all_noise),
            "ref": [
                x[0][1] if y[2] == "+" else rev_compl(x[0][1])
                for x, y in zip(chs, loci)
            ]
            + ini_all_noise,
            "alt": [
                x[1][1] if y[2] == "+" else rev_compl(x[1][1])
                for x, y in zip(chs, loci)
            ]
            + mut_all_noise,
            "qual": ["."] * len(loci) + ["."] * len(loci_all_noise),
            "filter": ["."] * len(loci) + ["."] * len(loci_all_noise),
            "info": ["None"] * len(loci) + ["None"] * len(loci_all_noise),
        }
    )
    df_vcf.to_csv(
        os.path.join(args.output, f"simulated_{sample_no}.vcf"),
        sep="\t",
        header=None,
        index=None,
    )

shutil.rmtree(temp_tosave)
